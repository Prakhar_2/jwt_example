
const { getJwt } = require('../services/getJwt')
const students = require('../student_db')

async function getJwtController(req, res) {
  const token = await getJwt(req, res)
  res.send({ token })
}

module.exports = { getJwtController }