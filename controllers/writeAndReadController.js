
const { getDetails } = require('../services/getStudentList')
const students = require('../student_db')
const fs = require('fs')
const path = require('path')

async function writeAndReadController(req, res) {

  const readFile = fs.createReadStream(path.join(__dirname + '../../read.txt'), 'utf-8')
  readFile.pipe(res)
}

module.exports = { writeAndReadController }