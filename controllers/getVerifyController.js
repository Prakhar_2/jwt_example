
const { getDetails } = require('../services/getStudentList')
const students = require('../student_db')

async function getVerifyController(req, res) {
  const details = await getDetails(req, res)
  if (details != undefined)
    res.send(details)
  else
    res.send({ message: 'No student found' })
}

module.exports = { getVerifyController }