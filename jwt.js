const express = require('express')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
const Router = require('./router/route')
const bodyParser = require('body-parser')
dotenv.config()

const PORT = process.env.PORT || 3000
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/', Router)

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})