const express = require('express')
const Router = express.Router()
const { getJwtController } = require('../controllers/getJwtController')
const { getVerifyController } = require('../controllers/getVerifyController')
const { writeAndReadController } = require('../controllers/writeAndReadController')
const { verifyToken } = require('../middleware/auth')

Router.post('/signup', getJwtController)
Router.post('/verify', verifyToken, getVerifyController)
Router.get('/fs', writeAndReadController)

module.exports = Router