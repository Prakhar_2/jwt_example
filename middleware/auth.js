const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')

dotenv.config()

const generateToken = async (id) => {
  let jwtSecretKey = process.env.JWT_SECRET_KEY;
  let data = {
    time: Date(),
    userId: id,
  }
  token = await jwt.sign(data, jwtSecretKey);
  return token;

}

async function verifyToken(req, res, next) {
  let tokenHeaderKey = process.env.TOKEN_HEADER_KEY;
  let jwtSecretKey = process.env.JWT_SECRET_KEY;
  const token = req.headers.authorization.split(' ')[1]

  // two ways to  pass token in first case postman Authorization select bearer and then paste token in text box
  //  in header  use your custom key tike prakhar_token and paste it  and you ll get everting in req.headers

  try {
    const verified = await jwt.verify(token, jwtSecretKey);
    // simple add user details after verifiaction for furthur process
    req.user = verified;
    next()
  } catch (err) {
    res.status(400).send('PLEASE_SEND_AUTHORIZED_TOKEN');
  }
}

module.exports = { generateToken, verifyToken }