const { generateToken } = require('../middleware/auth')
const students = require('../student_db')
const fs = require('fs')

async function getJwt(req, res) {
  const { id, userName, password, firstName, lastName } = req.body

  const details = { id: id, username: userName, password: password, firstName: firstName, lastName: lastName }
  fs.writeFileSync('../student_db.js', JSON.stringify(details));
  const token = await generateToken(id)
  return token
}

module.exports = { getJwt }