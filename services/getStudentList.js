const students = require('../student_db')

const getDetails = async (req, res) => {
  const { userId } = req.user

  console.log(`userId=-=-=-=-=-=: ${userId}`)

  try {
    let details = {}
    for (let student of students) {
      if (student.id == userId) {
        details = student
        break
      }
    }
    console.log('details--=-=-=-=-=: ', details)
    return details
  } catch (e) {
    console.log(e)
  }
}

module.exports = { getDetails }